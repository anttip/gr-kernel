/* -*- c++ -*- */
/* 
 * Copyright 2013 Antti Palosaari <crope@iki.fi>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "libv4l2_x_impl.h"

#include <sys/mman.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

static void xioctl(int fh, unsigned long int request, void *arg)
{
    int ret;

    do {
        ret = v4l2_ioctl(fh, request, arg);
    } while (ret == -1 && ((errno == EINTR) || (errno == EAGAIN)));
    if (ret == -1) {
        fprintf(stderr, "error %d, %s\n", errno, strerror(errno));
        exit(EXIT_FAILURE);
    }
}

// FIXME: leaks file handles on error

namespace gr {
  namespace linuxsdr {

    libv4l2_x::sptr
    libv4l2_x::make(const char *filename, const char *format)
    {
      return gnuradio::get_initial_sptr
        (new libv4l2_x_impl(filename, format));
    }

    /*
     * The private constructor
     */
    libv4l2_x_impl::libv4l2_x_impl(const char *filename, const char *format)
      : gr::sync_block("libv4l2_x",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, sizeof (gr_complex)))
    {
        int ret;
        struct v4l2_queryctrl queryctrl;

        libv4l2_x_impl::filename = filename;
        libv4l2_x_impl::format = format;
        libv4l2_x_impl::fd = -1;

        ret = v4l2_open(filename, O_RDWR | O_NONBLOCK, 0);
        if (ret < 0) {
            perror("Cannot open device");
            exit(EXIT_FAILURE);
        }

        libv4l2_x_impl::fd = ret;

        memset(&queryctrl, 0, sizeof(queryctrl));

        queryctrl.id = V4L2_CID_RF_TUNER_LNA_GAIN;
        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_QUERYCTRL, &queryctrl);
        if (ret == 0)
            libv4l2_x_impl::has_lna_gain = true;

        queryctrl.id = V4L2_CID_RF_TUNER_MIXER_GAIN;
        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_QUERYCTRL, &queryctrl);
        if (ret == 0)
            libv4l2_x_impl::has_mixer_gain = true;

        queryctrl.id = V4L2_CID_RF_TUNER_IF_GAIN;
        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_QUERYCTRL, &queryctrl);
        if (ret == 0)
            libv4l2_x_impl::has_if_gain = true;

        queryctrl.id = V4L2_CID_RF_TUNER_LNA_GAIN_AUTO;
        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_QUERYCTRL, &queryctrl);
        if (ret == 0)
            libv4l2_x_impl::has_lna_gain_auto = true;

        queryctrl.id = V4L2_CID_RF_TUNER_MIXER_GAIN_AUTO;
        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_QUERYCTRL, &queryctrl);
        if (ret == 0)
            libv4l2_x_impl::has_mixer_gain_auto = true;

        queryctrl.id = V4L2_CID_RF_TUNER_IF_GAIN_AUTO;
        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_QUERYCTRL, &queryctrl);
        if (ret == 0)
            libv4l2_x_impl::has_if_gain_auto = true;

        queryctrl.id = V4L2_CID_RF_TUNER_BANDWIDTH;
        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_QUERYCTRL, &queryctrl);
        if (ret == 0)
            libv4l2_x_impl::has_bandwidth = true;

        queryctrl.id = V4L2_CID_RF_TUNER_BANDWIDTH_AUTO;
        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_QUERYCTRL, &queryctrl);
        if (ret == 0)
            libv4l2_x_impl::has_bandwidth_auto = true;

        v4l2_close(libv4l2_x_impl::fd);
        libv4l2_x_impl::fd = -1;
    }

    /*
     * Our virtual destructor.
     */
    libv4l2_x_impl::~libv4l2_x_impl()
    {
    }

    void
    libv4l2_x_impl::set_samp_rate(double samp_rate)
    {
        struct v4l2_frequency frequency;

        printf("%s: samp_rate=%f\n", __func__, samp_rate);
        libv4l2_x_impl::samp_rate = samp_rate;

        if (libv4l2_x_impl::fd < 0) {
            return;
        }

        memset (&frequency, 0, sizeof(frequency));
        frequency.tuner = 0;
        frequency.type = V4L2_TUNER_ADC;
        frequency.frequency = samp_rate / 1;

        if (v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_S_FREQUENCY, &frequency) == -1)
            perror("VIDIOC_S_FREQUENCY");

        return;
    }

    void
    libv4l2_x_impl::set_center_freq(double freq)
    {
        struct v4l2_frequency frequency;

        printf("%s: frequency=%f\n", __func__, freq);
        libv4l2_x_impl::freq = freq;

        if (libv4l2_x_impl::fd < 0) {
            return;
        }

        memset (&frequency, 0, sizeof(frequency));
        frequency.tuner = 1;
        frequency.type = V4L2_TUNER_RF;
        frequency.frequency = freq;

        if (v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_S_FREQUENCY, &frequency) == -1)
            perror("VIDIOC_S_FREQUENCY");

        return;
    }

    void
    libv4l2_x_impl::set_bandwidth(double bandwidth)
    {
        int ret;
        struct v4l2_ext_control ext_ctrl[2];
        struct v4l2_ext_controls ext_ctrls;
        unsigned int count = 0;

        printf("%s: bandwidth=%f\n", __func__, bandwidth);
        libv4l2_x_impl::bandwidth = bandwidth;

        if (libv4l2_x_impl::fd < 0) {
            return;
        }

        if (bandwidth <= 0) {
            if (libv4l2_x_impl::has_bandwidth_auto == false) {
                printf("%s: bandwidth auto not supported\n", __func__);
            } else {
                memset (&ext_ctrl[0], 0, sizeof(ext_ctrl[0]));
                ext_ctrl[0].id = V4L2_CID_RF_TUNER_BANDWIDTH_AUTO;
                ext_ctrl[0].value = 1;
                count = 1;
            }
        } else {
            if (libv4l2_x_impl::has_bandwidth == false) {
                printf("%s: bandwidth not supported\n", __func__);
            } else {
                memset (&ext_ctrl[0], 0, sizeof(ext_ctrl[0]));
                ext_ctrl[0].id = V4L2_CID_RF_TUNER_BANDWIDTH;
                ext_ctrl[0].value = bandwidth;
                count = 1;
                if (libv4l2_x_impl::has_bandwidth_auto == true) {
                    memset (&ext_ctrl[1], 0, sizeof(ext_ctrl[1]));
                    ext_ctrl[1].id = V4L2_CID_RF_TUNER_BANDWIDTH_AUTO;
                    ext_ctrl[1].value = 0;
                    count = 2;
                }
            }
        }

        memset (&ext_ctrls, 0, sizeof(ext_ctrls));
        ext_ctrls.ctrl_class = V4L2_CID_RF_TUNER_CLASS;
        ext_ctrls.count = count;
        ext_ctrls.controls = ext_ctrl;

        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_S_EXT_CTRLS, &ext_ctrls);
        if (ret)
            perror("VIDIOC_S_EXT_CTRLS");

        return;
    }

    void
    libv4l2_x_impl::set_tuner_lna_gain(double gain)
    {
        int ret;
        struct v4l2_ext_control ext_ctrl[2];
        struct v4l2_ext_controls ext_ctrls;
        unsigned int count = 0;

        printf("%s: gain=%f\n", __func__, gain);
        libv4l2_x_impl::lna_gain = gain;

        if (libv4l2_x_impl::fd < 0) {
            return;
        }

        if (gain < 0) {
            if (libv4l2_x_impl::has_lna_gain_auto == false) {
                printf("%s: LNA AGC not supported\n", __func__);
            } else {
                memset (&ext_ctrl[0], 0, sizeof(ext_ctrl[0]));
                ext_ctrl[0].id = V4L2_CID_RF_TUNER_LNA_GAIN_AUTO;
                ext_ctrl[0].value = 1;
                count = 1;
            }
        } else {
            if (libv4l2_x_impl::has_lna_gain == false) {
                printf("%s: LNA gain not supported\n", __func__);
            } else {
                memset (&ext_ctrl[0], 0, sizeof(ext_ctrl[0]));
                ext_ctrl[0].id = V4L2_CID_RF_TUNER_LNA_GAIN;
                ext_ctrl[0].value = gain;
                count = 1;
                if (libv4l2_x_impl::has_lna_gain_auto == true) {
                    memset (&ext_ctrl[1], 0, sizeof(ext_ctrl[1]));
                    ext_ctrl[1].id = V4L2_CID_RF_TUNER_LNA_GAIN_AUTO;
                    ext_ctrl[1].value = 0;
                    count = 2;
                }
            }
        }

        memset (&ext_ctrls, 0, sizeof(ext_ctrls));
        ext_ctrls.ctrl_class = V4L2_CID_RF_TUNER_CLASS;
        ext_ctrls.count = count;
        ext_ctrls.controls = ext_ctrl;

        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_S_EXT_CTRLS, &ext_ctrls);
        if (ret)
            perror("VIDIOC_S_EXT_CTRLS");

        return;
    }

    void
    libv4l2_x_impl::set_tuner_mixer_gain(double mixer_gain)
    {
        int ret;
        struct v4l2_ext_control ext_ctrl[2];
        struct v4l2_ext_controls ext_ctrls;
        unsigned int count = 0;

        printf("%s: mixer_gain=%f\n", __func__, mixer_gain);
        libv4l2_x_impl::mixer_gain = mixer_gain;

        if (libv4l2_x_impl::fd < 0) {
            return;
        }

        if (mixer_gain < 0) {
            if (libv4l2_x_impl::has_mixer_gain_auto == false) {
                printf("%s: Mixer AGC not supported\n", __func__);
            } else {
                memset (&ext_ctrl[0], 0, sizeof(ext_ctrl[0]));
                ext_ctrl[0].id = V4L2_CID_RF_TUNER_MIXER_GAIN_AUTO;
                ext_ctrl[0].value = 1;
                count = 1;
            }
        } else {
            if (libv4l2_x_impl::has_mixer_gain == false) {
                printf("%s: Mixer gain not supported\n", __func__);
            } else {
                memset (&ext_ctrl[0], 0, sizeof(ext_ctrl[0]));
                ext_ctrl[0].id = V4L2_CID_RF_TUNER_MIXER_GAIN;
                ext_ctrl[0].value = mixer_gain;
                count = 1;
                if (libv4l2_x_impl::has_mixer_gain_auto == true) {
                    memset (&ext_ctrl[1], 0, sizeof(ext_ctrl[1]));
                    ext_ctrl[1].id = V4L2_CID_RF_TUNER_MIXER_GAIN_AUTO;
                    ext_ctrl[1].value = 0;
                    count = 2;
                }
            }
        }

        memset (&ext_ctrls, 0, sizeof(ext_ctrls));
        ext_ctrls.ctrl_class = V4L2_CID_RF_TUNER_CLASS;
        ext_ctrls.count = count;
        ext_ctrls.controls = ext_ctrl;

        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_S_EXT_CTRLS, &ext_ctrls);
        if (ret)
            perror("VIDIOC_S_EXT_CTRLS");

        return;
    }

    void
    libv4l2_x_impl::set_tuner_if_gain(double gain)
    {
        int ret;
        struct v4l2_ext_control ext_ctrl[2];
        struct v4l2_ext_controls ext_ctrls;
        unsigned int count = 0;

        printf("%s: gain=%f\n", __func__, gain);
        libv4l2_x_impl::if_gain = gain;

        if (libv4l2_x_impl::fd < 0) {
            return;
        }

        if (gain < 0) {
            if (libv4l2_x_impl::has_if_gain_auto == false) {
                printf("%s: IF AGC not supported\n", __func__);
            } else {
                memset (&ext_ctrl[0], 0, sizeof(ext_ctrl[0]));
                ext_ctrl[0].id = V4L2_CID_RF_TUNER_IF_GAIN_AUTO;
                ext_ctrl[0].value = 1;
                count = 1;
            }
        } else {
            if (libv4l2_x_impl::has_if_gain == false) {
                printf("%s: IF gain not supported\n", __func__);
            } else {
                memset (&ext_ctrl[0], 0, sizeof(ext_ctrl[0]));
                ext_ctrl[0].id = V4L2_CID_RF_TUNER_IF_GAIN;
                ext_ctrl[0].value = gain;
                count = 1;
                if (libv4l2_x_impl::has_if_gain_auto == true) {
                    memset (&ext_ctrl[1], 0, sizeof(ext_ctrl[1]));
                    ext_ctrl[1].id = V4L2_CID_RF_TUNER_IF_GAIN_AUTO;
                    ext_ctrl[1].value = 0;
                    count = 2;
                }
            }
        }

        memset (&ext_ctrls, 0, sizeof(ext_ctrls));
        ext_ctrls.ctrl_class = V4L2_CID_RF_TUNER_CLASS;
        ext_ctrls.count = count;
        ext_ctrls.controls = ext_ctrl;

        ret = v4l2_ioctl(libv4l2_x_impl::fd, VIDIOC_S_EXT_CTRLS, &ext_ctrls);
        if (ret)
            perror("VIDIOC_S_EXT_CTRLS");

        return;
    }

    bool
    libv4l2_x_impl::start()
    {
        struct v4l2_format fmt;
        struct v4l2_buffer buf;
        struct v4l2_requestbuffers req;
        enum v4l2_buf_type type;
        unsigned int i;
        const char *fourcc = libv4l2_x_impl::format;

        printf("%s\n", __func__);

        recebuf_len = 0;

        libv4l2_x_impl::fd = v4l2_open(filename, O_RDWR | O_NONBLOCK, 0);
        if (libv4l2_x_impl::fd < 0) {
            perror("Cannot open device");
            exit(EXIT_FAILURE);
        }

        pixelformat = v4l2_fourcc(fourcc[0], fourcc[1], fourcc[2], fourcc[3]);
        printf("Requested input format: %4.4s\n", (char *)&pixelformat);

        CLEAR(fmt);
        fmt.type = V4L2_BUF_TYPE_SDR_CAPTURE;
        fmt.fmt.sdr.pixelformat = pixelformat;
        xioctl(libv4l2_x_impl::fd, VIDIOC_S_FMT, &fmt);

        if (fmt.fmt.sdr.pixelformat != pixelformat) {
            printf("Device didn't accept input format", (char *)&fmt.fmt.sdr.pixelformat);
            exit(EXIT_FAILURE);
        } else {
            printf("Selected input format: %4.4s\n", (char *)&fmt.fmt.sdr.pixelformat);
        }

        libv4l2_x_impl::set_samp_rate(libv4l2_x_impl::samp_rate);
        libv4l2_x_impl::set_center_freq(libv4l2_x_impl::freq);
        libv4l2_x_impl::set_bandwidth(libv4l2_x_impl::bandwidth);
        libv4l2_x_impl::set_tuner_lna_gain(libv4l2_x_impl::lna_gain);
        libv4l2_x_impl::set_tuner_mixer_gain(libv4l2_x_impl::mixer_gain);
        libv4l2_x_impl::set_tuner_if_gain(libv4l2_x_impl::if_gain);

        CLEAR(req);
        req.count = 8;
        req.type = V4L2_BUF_TYPE_SDR_CAPTURE;
        req.memory = V4L2_MEMORY_MMAP;
        xioctl(libv4l2_x_impl::fd, VIDIOC_REQBUFS, &req);

        buffers = (struct buffer*) calloc(req.count, sizeof(*buffers));
        for (n_buffers = 0; n_buffers < req.count; n_buffers++) {
            CLEAR(buf);
            buf.type = V4L2_BUF_TYPE_SDR_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = n_buffers;
            xioctl(libv4l2_x_impl::fd, VIDIOC_QUERYBUF, &buf);

            buffers[n_buffers].length = buf.length;
            buffers[n_buffers].start = v4l2_mmap(NULL, buf.length,
                    PROT_READ | PROT_WRITE, MAP_SHARED,
                    libv4l2_x_impl::fd, buf.m.offset);

            if (buffers[n_buffers].start == MAP_FAILED) {
                perror("mmap");
                exit(EXIT_FAILURE);
            }
        }

        for (i = 0; i < n_buffers; i++) {
            CLEAR(buf);
            buf.type = V4L2_BUF_TYPE_SDR_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = i;
            xioctl(libv4l2_x_impl::fd, VIDIOC_QBUF, &buf);
        }

        // start streaming
        type = V4L2_BUF_TYPE_SDR_CAPTURE;
        xioctl(libv4l2_x_impl::fd, VIDIOC_STREAMON, &type);

        return true;
    }

    bool
    libv4l2_x_impl::stop()
    {
        unsigned int i;
        enum v4l2_buf_type type;

        printf("%s\n", __func__);

        // stop streaming
        type = V4L2_BUF_TYPE_SDR_CAPTURE;
        xioctl(libv4l2_x_impl::fd, VIDIOC_STREAMOFF, &type);

        for (i = 0; i < n_buffers; i++)
            v4l2_munmap(buffers[i].start, buffers[i].length);

        v4l2_close(libv4l2_x_impl::fd);

        return true;
    }

    int
    libv4l2_x_impl::work(int noutput_items,
                         gr_vector_const_void_star &input_items,
                         gr_vector_void_star &output_items)
    {
        gr_complex *out = (gr_complex *) output_items[0];
        int ret;
        struct timeval tv;
        struct v4l2_buffer buf;
        fd_set fds;
        unsigned int i, items = 0;
        float *fptr = (float *) output_items[0];

process_buf:
        /* process received mmap buffer */
        uint8_t *u8src = (uint8_t *) recebuf_ptr;
        uint16_t *u16src = (uint16_t *) recebuf_ptr;
        int8_t *s8src = (int8_t *) recebuf_ptr;
        int16_t *s16src = (int16_t *) recebuf_ptr;

        while (recebuf_len) {
            if (pixelformat == V4L2_SDR_FMT_CU8) {
                *fptr++ = (*u8src++ - 127.5f) / 127.5f;
                *fptr++ = (*u8src++ - 127.5f) / 127.5f;
                recebuf_len -= 2;
                items++;
                recebuf_ptr = u8src;
            } else if (pixelformat == V4L2_SDR_FMT_CU16LE) {
                *fptr++ = (*u16src++ - 32767.5f) / 32767.5f;
                *fptr++ = (*u16src++ - 32767.5f) / 32767.5f;
                recebuf_len -= 4;
                items++;
                recebuf_ptr = u16src;
            } else if (pixelformat == V4L2_SDR_FMT_CS8) {
                *fptr++ = (*s8src++ + 0.5f) / 127.5f;
                *fptr++ = (*s8src++ + 0.5f) / 127.5f;
                recebuf_len -= 2;
                items++;
                recebuf_ptr = s8src;
            } else if (pixelformat == V4L2_SDR_FMT_CS14LE) {
                /* 14-bit signed to 32-bit float */
                struct {signed int x:14;} s; /* sign extension 14-bit */
                s.x = *s16src++;
                *fptr++ = (s.x + 0.5f) / 8192.5f;
                s.x = *s16src++;
                *fptr++ = (s.x + 0.5f) / 8192.5f;
                recebuf_len -= 4;
                items++;
                recebuf_ptr = s16src;
            } else if (pixelformat == V4L2_SDR_FMT_RU12LE) {
                /* FIXME: we output real samples from complex source */
                *fptr++ = (*u16src++ - 2047.5f) / 2047.5f;
                *fptr++ = 0.0f; /* leave imaginary part empty */
                recebuf_len -= 2;
                items++;
                recebuf_ptr = u16src;
            } else {
                recebuf_len = 0;
            }

            if (items == noutput_items)
                break;
        }

        /* enqueue mmap buf after it is processed */
        if (recebuf_len == 0 && items != 0) {
            CLEAR(buf);
            buf.type = V4L2_BUF_TYPE_SDR_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = recebuf_mmap_index;
            xioctl(libv4l2_x_impl::fd, VIDIOC_QBUF, &buf);
        }

        /* signal DSP we have some samples to offer */
        if (items)
            return items;

        /* Read data from device */
        do {
            FD_ZERO(&fds);
            FD_SET(libv4l2_x_impl::fd, &fds);

            // Timeout
            tv.tv_sec = 2;
            tv.tv_usec = 0;

            ret = select(libv4l2_x_impl::fd + 1, &fds, NULL, NULL, &tv);
        } while ((ret == -1 && (errno = EINTR)));
        if (ret == -1) {
            perror("select");
            return errno;
        }

        /* dequeue mmap buf (receive data) */
        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_SDR_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        xioctl(libv4l2_x_impl::fd, VIDIOC_DQBUF, &buf);

        /* store buffer in order to handle it during next call */
        recebuf_ptr = buffers[buf.index].start;
        recebuf_len = buf.bytesused;
        recebuf_mmap_index = buf.index;
        /* FIXME: */
        goto process_buf;

        // Tell runtime system how many output items we produced.
        return 0;
    }

  } /* namespace linuxsdr */
} /* namespace gr */

