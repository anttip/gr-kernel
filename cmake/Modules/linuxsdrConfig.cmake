INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_LINUXSDR linuxsdr)

FIND_PATH(
    LINUXSDR_INCLUDE_DIRS
    NAMES linuxsdr/api.h
    HINTS $ENV{LINUXSDR_DIR}/include
        ${PC_LINUXSDR_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    LINUXSDR_LIBRARIES
    NAMES gnuradio-linuxsdr
    HINTS $ENV{LINUXSDR_DIR}/lib
        ${PC_LINUXSDR_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LINUXSDR DEFAULT_MSG LINUXSDR_LIBRARIES LINUXSDR_INCLUDE_DIRS)
MARK_AS_ADVANCED(LINUXSDR_LIBRARIES LINUXSDR_INCLUDE_DIRS)

