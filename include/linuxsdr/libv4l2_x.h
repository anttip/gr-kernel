/* -*- c++ -*- */
/* 
 * Copyright 2013 Antti Palosaari <crope@iki.fi>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_LINUXSDR_LIBV4L2_X_H
#define INCLUDED_LINUXSDR_LIBV4L2_X_H

#include <linuxsdr/api.h>
#include <gnuradio/sync_block.h>

#include <libv4l2.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/videodev2.h>

namespace gr {
  namespace linuxsdr {

    /*!
     * \brief <+description of block+>
     * \ingroup linuxsdr
     *
     */
    class LINUXSDR_API libv4l2_x : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<libv4l2_x> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of linuxsdr::libv4l2_x.
       *
       * To avoid accidental use of raw pointers, linuxsdr::libv4l2_x's
       * constructor is in a private implementation
       * class. linuxsdr::libv4l2_x::make is the public interface for
       * creating new instances.
       */
       static sptr make(const char *filename, const char *format);

       virtual void set_samp_rate(double samp_rate) = 0;
       virtual void set_center_freq(double freq) = 0;
       virtual void set_bandwidth(double bandwidth) = 0;
       virtual void set_tuner_lna_gain(double gain) = 0;
       virtual void set_tuner_mixer_gain(double gain) = 0;
       virtual void set_tuner_if_gain(double gain) = 0;
    };

  } // namespace linuxsdr
} // namespace gr

#endif /* INCLUDED_LINUXSDR_LIBV4L2_X_H */

